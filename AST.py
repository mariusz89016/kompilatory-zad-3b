class Node(object):
    def accept(self, visitor):
        return visitor.visit(self)

    def __init__(self, lineno=-1):
        self.lineno = lineno


class List(Node):
    def __init__(self, *args, **kwargs):
        super(List, self).__init__(*args, **kwargs)
        self.list = []
        self.children = self.list

    def add(self, element):
        self.list.append(element)
        return self


class Program(Node):
    def __init__(self, declarations, fundefs, instructions, *args, **kwargs):
        super(Program, self).__init__(*args, **kwargs)
        self.declarations = declarations
        self.fundefs = fundefs
        self.instructions = instructions

        self.children = (declarations, fundefs, instructions)


class Declaration(Node):
    def __init__(self, type, inits, *args, **kwargs):
        super(Declaration, self).__init__(*args, **kwargs)
        self.type = type
        self.inits = inits


class Init(Node):
    def __init__(self, id, expression, *args, **kwargs):
        super(Init, self).__init__(*args, **kwargs)
        self.id = id
        self.expression = expression


class PrintInstruction(Node):
    def __init__(self, expression, *args, **kwargs):
        super(PrintInstruction, self).__init__(*args, **kwargs)
        self.expression = expression


class LabeledInstruction(Node):
    def __init__(self, id, instruction, *args, **kwargs):
        super(LabeledInstruction, self).__init__(*args, **kwargs)
        self.id = id
        self.instruction = instruction


class Assignment(Node):
    def __init__(self, id, expression, *args, **kwargs):
        super(Assignment, self).__init__(*args, **kwargs)
        self.id = id
        self.expression = expression


class ChoiceInstructionIfElse(Node):
    def __init__(self, condition, if_instruction, else_instruction, *args, **kwargs):
        super(ChoiceInstructionIfElse, self).__init__(*args, **kwargs)
        self.condition = condition
        self.if_instruction = if_instruction
        self.else_instruction = else_instruction


class ChoiceInstructionIf(Node):
    def __init__(self, condition, if_instruction, *args, **kwargs):
        super(ChoiceInstructionIf, self).__init__(*args, **kwargs)
        self.condition = condition
        self.if_instruction = if_instruction


class WhileInstruction(Node):
    def __init__(self, condition, instruction, *args, **kwargs):
        super(WhileInstruction, self).__init__(*args, **kwargs)
        self.condition = condition
        self.instruction = instruction


class RepeatInstruction(Node):
    def __init__(self, instructions, condition, *args, **kwargs):
        super(RepeatInstruction, self).__init__(*args, **kwargs)
        self.instructions = instructions
        self.condition = condition


class ReturnInstruction(Node):
    def __init__(self, expression, *args, **kwargs):
        super(ReturnInstruction, self).__init__(*args, **kwargs)
        self.expression = expression

        self.children = (expression, )


class ContinueInstruction(Node):
    def __init__(self, *args, **kwargs):
        super(ContinueInstruction, self).__init__(*args, **kwargs)
        self.children = ()


class BreakInstruction(Node):
    def __init__(self, *args, **kwargs):
        super(BreakInstruction, self).__init__(*args, **kwargs)
        self.children = ()


class CompoundInstruction(Node):
    def __init__(self, declarations, instructions, *args, **kwargs):
        super(CompoundInstruction, self).__init__(*args, **kwargs)
        self.declarations = declarations
        self.instructions = instructions

        self.children = (declarations, instructions)


class Condition(Node):
    def __init__(self, expression, *args, **kwargs):
        super(Condition, self).__init__(*args, **kwargs)
        self.expression = expression


class Const(Node):
    def __init__(self, const, *args, **kwargs):
        super(Const, self).__init__(*args, **kwargs)
        self.const = const

    def __str__(self):
        return str(self.const)


class Integer(Const):
    pass


class Float(Const):
    pass


class String(Const):
    pass


class Id(Const):
    pass


class BinaryExpression(Node):
    def __init__(self, left, op, right, *args, **kwargs):
        super(BinaryExpression, self).__init__(*args, **kwargs)
        self.op = op
        self.left = left
        self.right = right


class ParenthesisExpression(Node):
    def __init__(self, expression, *args, **kwargs):
        super(ParenthesisExpression, self).__init__(*args, **kwargs)
        self.expression = expression


class FunctionCall(Node):
    def __init__(self, id, instruction, *args, **kwargs):
        super(FunctionCall, self).__init__(*args, **kwargs)
        self.id = id
        self.instruction = instruction


class Fundef(Node):
    def __init__(self, type, id, args_list, compound_instr, *args, **kwargs):
        super(Fundef, self).__init__(*args, **kwargs)
        self.type = type
        self.id = id
        self.args_list = args_list
        self.compound_instr = compound_instr


class Argument(Node):
    def __init__(self, type, id, *args, **kwargs):
        super(Argument, self).__init__(*args, **kwargs)
        self.type = type
        self.id = id